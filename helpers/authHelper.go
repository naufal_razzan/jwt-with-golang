package helpers

import (
	"errors"
	"log"

	"github.com/gin-gonic/gin"
)

func CheckUserType(c *gin.Context, role string) (err error) {
	userType := c.GetString("user_type")
	err = nil

	if userType != role {
		log.Println("user type: " + userType + " role: " + role)
		err = errors.New("Unauthorized to access this resources")
		return err
	}

	return err
}

func MatchUserTypeToUid(c *gin.Context, userId string) (err error) {
	userType := c.GetString("user_type")
	uid := c.GetString("uid")
	err = nil

	if userType == "USER" && uid != userId {
		err = errors.New("Unauthorized to access this resources")
		return err
	}

	err = CheckUserType(c, userType)

	return err
}
